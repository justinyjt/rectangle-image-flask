import cv2 as cv
import math
import time
from flask import redirect, url_for

def get_duration(filename):
    vidcap = cv.VideoCapture('/Users/justinyip/Desktop/IMDA/flask/proj1/static/' + filename)
    total_frames = vidcap.get(7) # total frame count
    fps = vidcap.get(cv.CAP_PROP_FPS) # Frames per second
    return int(total_frames / fps)

def gen_frames(filename, pt1, pt2):
    vidcap = cv.VideoCapture('/Users/justinyip/Desktop/IMDA/flask/proj1/static/' + filename)
    
    total_frames = vidcap.get(7) # total frame count
    width= int(vidcap.get(3)) # width
    height= int(vidcap.get(4)) # height
    fps = vidcap.get(cv.CAP_PROP_FPS) # Frames per second
    success, image = vidcap.read()

    while True:
        success, image = vidcap.read()
        if not success:
            continue
        frame_id = vidcap.get(1) # Current frame no.
        elasped_time = frame_id / fps # Current time of video
        if math.floor(elasped_time) % 2 == 0: # Red rectangle for even seconds
            cv.rectangle(image, pt1, pt2, (0, 0, 255))
        if math.floor(elasped_time) % 2 != 0: # Blue rectangle for odd seconds
            cv.rectangle(image, pt1, pt2, (255, 0, 0)) 
        # time.sleep(int(video_time/total_frames))
        # writer.write(image)
        # cv.imshow("Frame", image)
        # cv.waitKey(int(video_time/total_frames * 100))
        ret, buffer = cv.imencode('.jpg', image)
        image = buffer.tobytes()
        cv.waitKey(25)
        yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + image + b'\r\n')  # concat frame one by one and show result
        

    vidcap.release()
    cv.destroyAllWindows()




