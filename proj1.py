from flask import Flask, request, url_for, render_template, \
    redirect, flash, session, Response, stream_with_context
from flask_caching import Cache
from werkzeug.utils import secure_filename
from datetime import time, date
import cv2 as cv
import os
import math
from video import gen_frames
# from flask_wtf import FlaskForm
# from wtforms import validators, SubmitField, SelectField
# from wtforms.validators import DataRequired
# from wtforms.fields.html5 import DateField
# from wtforms_components import DateRange, TimeRange



app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = '/Users/justinyip/Desktop/IMDA/flask/proj1/static'
app.secret_key = 'jd&(I_(][/:<aefa)['
frames = None
### Attempt at using wtforms
# class InfoForm(FlaskForm):
    
#     start_date = DateField('Start date', validators=[DataRequired(), DateRange(min=date(2019,1,15))])
#     start_time = SelectField(u'Start time', 
#         choices=[('0700', '0700'), ('0800', '0800'),
#             ('0900', '0900'), ('1000', '1000'),
#             ('1100', '1100'), ('1200', '1200'),
#             ('1300', '1300'), ('1400', '1400'),
#             ('1500', '1500'), ('1600', '1600'),
#             ('1700', '1700'), ('1800', '1800'),
#             ('1900', '1900'), ('2000', '2000'),
#             ('2100', '2100'), ('2200', '2200')], 
#         validators=[DataRequired()])
#     end_date = DateField('End date', validators=[DataRequired(), DateRange(min=date(2019,1,15))])
#     end_time = SelectField(u'Start time', 
#         choices=[('0700', '0700'), ('0800', '0800'),
#             ('0900', '0900'), ('1000', '1000'),
#             ('1100', '1100'), ('1200', '1200'),
#             ('1300', '1300'), ('1400', '1400'),
#             ('1500', '1500'), ('1600', '1600'),
#             ('1700', '1700'), ('1800', '1800'),
#             ('1900', '1900'), ('2000', '2000'),
#             ('2100', '2100'), ('2200', '2200')], 
#         validators=[DataRequired()])
#     submit = SubmitField('Submit')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login_attempt')
def login_attempt():
    return render_template('login.html')

@app.route('/login', methods = [ "POST", "GET" ])
def login():
    if request.method == 'POST':
        user = request.form['nm']
        return redirect(url_for('home', name = user))
    else:
        user = request.args.get('nm')
        return redirect(url_for('home', name = user))

@app.route('/home/<name>')
def home(name):
    return render_template('home.html', user = name)

@app.route('/home/upload_image')
def upload_image():
    return render_template('upload_image.html')

# Draws rectangle around uploaded image before sending it to html
@app.route('/image_uploader', methods = [ 'GET', 'POST' ])
def image_uploader():
    try:
        if request.method == 'POST':
            
            flash('File uploaded successfully')
            f = request.files['file']
            f_name = f.filename
            path = os.path.join(app.config['UPLOAD_FOLDER'], f_name)
            f.save(path)
            x1 = request.form['x1-coord']
            y1 = request.form['y1-coord']
            x2 = request.form['x2-coord']
            y2 = request.form['y2-coord']
            box_colour = request.form['dropdown']

            red = (0, 0, 255)
            green = (0, 255, 0)
            blue = (255, 0 ,0)

            colours = {
                "RED" : red,
                "GREEN" : green,
                "BLUE" :blue
            }
            
            new_path = os.path.join(app.config['UPLOAD_FOLDER'], 'rectangle_images')
            timestr = time.strftime("%Y%m%d-%H%M%S")
            # adds time stamp to the abck of filename to prevent caching issues
            new_name = f_name[0:f_name.index('.')] + "_" + timestr + f_name[f_name.index('.'):] 
            rec = cv.imread(path)
            os.chdir(new_path)
            rec = cv.rectangle(rec, (int(x1),int(y1)), (int(x2),int(y2)), colours[box_colour], 3)
            cv.imwrite(new_name, rec)
            return render_template('rectangle.html', image=os.path.join(new_path, new_name), 
            img_name=new_name)
    except FileNotFoundError:
        flash('Invalid Upload! Please try again.')
        return redirect(url_for('upload_image'))

@app.route("/home/sports_sg")
def sports_sg():
    return render_template("date_input.html")

def date_validity_checker(start_date, start_time, end_date, end_time):
    if start_date > end_date or (start_date == end_date and start_time > end_time):
        raise ValueError
    
@app.route("/generate_prediction", methods=['POST', 'GET'])
def sports_trafficker():
    venue = request.form['venue']
    start_date = request.form['start_date']
    start_time = request.form['start_time']
    end_date = request.form['end_date']
    end_time = request.form['end_time']
    
    try:
        date_validity_checker(start_date, start_time, end_date, end_time)
        # results.html is an empty html file for you to generate the results.
        return render_template("results.html", venue=venue, start_date=start_date, start_time=start_time, 
            end_date=end_date, end_time=end_time) 
    except ValueError:
        flash('Invalid datetime input! Please try again.')
        return redirect(url_for('sports_sg'))

@app.route("/home/upload_video")
def upload_video():
    return render_template('upload_video.html')

# # # Draws rectangle around uploaded image before sending it to html
# @app.route('/video_boxer/<frames>')
# def video_boxer(frames):
#     print(frames)
#     return render_template('video_boxer.html', frames=frames)

    
@app.route('/video_upload', methods = [ 'GET', 'POST' ])
def video_upload():
    try:
        if request.method == 'POST':
            
            flash('File uploaded successfully')
            f = request.files['file']
            f_name = f.filename
            path = os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(f_name))
            f.save(path)
            x1 = request.form['x1-coord']
            y1 = request.form['y1-coord']
            x2 = request.form['x2-coord']
            y2 = request.form['y2-coord']
            pt1 = (int(x1), int(y1))
            pt2 = (int(x2), int(y2))
            global frames
            frames = gen_frames(f_name, pt1, pt2)
            return render_template('video_boxer.html')
    
    except FileNotFoundError:
        flash('Invalid Upload! Please try again.')
        return redirect(url_for('upload_video'))

@app.route('/video_feed')
def video_feed():
    return Response(frames, mimetype='multipart/x-mixed-replace; boundary=frame')

### Wtforms attempt
# @app.route("/success/sports_hall")
# def sports_hall():
#     form = InfoForm()
#     if form.validate_on_submit():
#         session['Start date'] = form.start_date.data
#         session['Start time'] = form.start_time.data
#         session['End date'] = form.end_date.data
#         session['End time'] = form.end_time.data
#         return redirect(url_for('results'))
#     return render_template("sports.html", form=form)

# @app.route("/results")
# def results():
#     return render_template("result.html")


app.run(debug=True)
